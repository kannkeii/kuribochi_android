package ambloom.kuribochi_android.model;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.text.format.Time;
import android.widget.Toast;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import ambloom.kuribochi_android.R;

/**
 * Created by kannkeii on 2015/12/28.
 */
public class MyFragment2 extends Fragment {

    private Context mContext;
    private ListView listView;
    private Button mButtom,mButtom2;
    private ArrayList<String> usermessagelist,objIDList;
    private LayoutInflater inflater;
    private DataAdapter dataAdapter;
    private String mUserID;
    private int index = 0;

//    public MyFragment2(String _userID)
//    {
//        mUserID = _userID;
//    }

    public MyFragment2(){}

    public void setUserid(String _id)
    {
        mUserID = _id;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_all2, container, false);
        this.mContext = this.getActivity().getApplicationContext();
        this.listView = (ListView) view.findViewById(R.id.id_listview2);
        this.mButtom = (Button)view.findViewById(R.id.id_layout_allcnt);
        this.mButtom2 = (Button)view.findViewById(R.id.id_Button_share);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                index = position;
                CreatDialog();
                return false;
            }
        });



        ParseQuery<ParseObject> query = ParseQuery.getQuery("GoodJob");
        query.whereEqualTo("userName", mUserID);
        query.orderByDescending("createdAt");

        //query.whereGreaterThanOrEqualTo("createdAt", date);//http://murayama.hatenablog.com/entry/2013/11/30/093741
        //query.addDescendingOrder("goodJob");
        

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    ArrayList<Integer> pluspoint = new ArrayList<Integer>();
                    usermessagelist = new ArrayList<String>();
                    objIDList = new ArrayList<String>();
                    dataAdapter = new DataAdapter();
                    for (ParseObject parseObject : objects) {
                        String s1 = parseObject.get("text").toString();
                        String s2 = parseObject.get("goodJob").toString();
                        String s3 = parseObject.get("sex").toString();
                        String s4 = parseObject.get("nickName").toString();
                        if(TextUtils.isEmpty(s4))s4 = "(´・ω・｀)さん";
                        //else s4+="さん";
                        Date createtime = parseObject.getCreatedAt();
                        String s5 = getDaytoChristmas(createtime);
                        String sid = parseObject.getObjectId();

                        //String s5 = parseObject.getCreatedAt().toString();

                        usermessagelist.add(s1);
                        usermessagelist.add(s2);
                        usermessagelist.add(s3);
                        usermessagelist.add(s4);
                        usermessagelist.add(s5);
                        objIDList.add(sid);
                        pluspoint.add(new Integer(s2).intValue());
                }

                    int num = 0;
                    for(int n : pluspoint)
                    {
                        num+=n;
                    }

                    mButtom.setText(Integer.toString(num));
                    listView.setAdapter(dataAdapter);
                }
            }

        });

        ShareLine();

        return view;
    }

    private void ShareLine()
    {
        mButtom2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            List<Intent> targetShareIntents=new ArrayList<Intent>();
            Intent shareIntent=new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            List<ResolveInfo> resInfos=getActivity().getPackageManager().queryIntentActivities(shareIntent, 0);
            if(!resInfos.isEmpty()){
                //System.out.println("Have package");
                for(ResolveInfo resInfo : resInfos){
                    String packageName=resInfo.activityInfo.packageName;
                    //System.out.println("Package Name=>"+ packageName);
                    if(packageName.contains("com.twitter.android") || packageName.contains("com.facebook.katana") || packageName.contains("com.kakao.story") || packageName.contains("jp.naver.line.android") || packageName.contains("com.google.android.gm")){
                        Intent intent=new Intent();
                        intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT, "合計で"+mButtom.getText()+"個の(´・ω・｀)を獲得したよ！...(´；ω；｀) URL");
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                        intent.setPackage(packageName);
                        targetShareIntents.add(intent);
                    }
                }
                if(!targetShareIntents.isEmpty()){
                    //System.out.println("Have Intent");
                    Intent chooserIntent=Intent.createChooser(targetShareIntents.remove(0), "アプリを選択してシェール");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                    startActivity(chooserIntent);
                }else{
                    //System.out.println("Do not Have Intent");
                    Toast.makeText(getActivity(),"シェールできるアプリをインストールしてください",Toast.LENGTH_LONG).show();
                    //Uri uri = Uri.parse("market://details?id="+);
                    //Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                    //startActivity(intent);
                }
            }
            /*全部アプリでshare*/
//            Intent intent = new Intent(Intent.ACTION_SEND);
//            intent.putExtra(Intent.EXTRA_TEXT,"合計でn個の(´・ω・｀)を獲得したよ！...(´；ω；｀) URL");
//            intent.setType("text/plain");
//            startActivity(intent);

            /*ツイッターのSDK*/
//            try
//            {
//                // Check if the Twitter app is installed on the phone.
//                getActivity().getPackageManager().getPackageInfo("com.twitter.android", 0);
//                Intent intent = new Intent(Intent.ACTION_SEND);
//                intent.setClassName("com.twitter.android", "com.twitter.android.composer.ComposerActivity");
//                intent.setType("text/plain");
//                intent.putExtra(Intent.EXTRA_TEXT, "Your text");
//                startActivity(intent);
//
//            }
//            catch (Exception e)
//            {
//                Toast.makeText(getActivity(),"Twitter is not installed on this device",Toast.LENGTH_LONG).show();
//
//            }
        }
    });
    }

    public static String getDaytoChristmas(Date _day)
    {
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        long day = 0;
        String returnStr = null;
        try{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(_day);
            Date christmasDay = myFormatter.parse(calendar.get(Calendar.YEAR) + "-12-25");

            day = (_day.getTime()-christmasDay.getTime())/(24*60*60*1000);

            if(day>0)
            {
                returnStr = "クリスマス経過して" + Math.abs(day) + "日";
            }else if(day<0)
            {
                returnStr = "クリスマスまで" + Math.abs(day) + "日";
            }else if(day == 0)
            {
                returnStr = "クリスマスディナー";
            }

        } catch (java.text.ParseException e) {
            return "";
        }
        return returnStr;
    }

    public class DataAdapter extends BaseAdapter {

        public DataAdapter() {

            inflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return usermessagelist.size()/5;
        }

        @Override
        public Object getItem(int position) {
            return usermessagelist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            ViewHolder viewHolder = null;

            if (view == null) {
                viewHolder = new ViewHolder();

                view = inflater.inflate(R.layout.layout_item, parent, false);
                viewHolder.content = (TextView) view.findViewById(R.id.id_messagetext);
                viewHolder.goodjobcnt = (TextView)view.findViewById(R.id.id_goodcnt);
                viewHolder.touch =  (ImageView) view.findViewById(R.id.id_goodbuttom);
                viewHolder.icon = (ImageView)view.findViewById(R.id.id_userid);
                viewHolder.username = (TextView)view.findViewById(R.id.id_username);
                viewHolder.datetime = (TextView)view.findViewById(R.id.id_ymd);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder)view.getTag();

            }

            int cnt = position + position * 4;//ViewHolder内のメンバー数-1(iconを抜いて)-1

            viewHolder.content.setText(usermessagelist.get(cnt));
            viewHolder.touch.setImageResource(R.drawable.goodjobl2x);
            viewHolder.goodjobcnt.setText(usermessagelist.get(cnt + 1));
            setIcon(cnt, viewHolder);
            viewHolder.username.setText(usermessagelist.get(cnt + 3));
            viewHolder.datetime.setText(usermessagelist.get(cnt+4));
            return view;
        }

        private void setIcon(int _cnt,ViewHolder _viewHolder)
        {
            String s = usermessagelist.get(_cnt + 2);
            int cnt = Integer.parseInt(s);
            switch (cnt)
            {
                case 0:
                    _viewHolder.icon.setImageResource(R.drawable.btn_man_activel2x);
                    break;
                case 1:
                    _viewHolder.icon.setImageResource(R.drawable.btn_woman_activel2x);
                    break;
                case 2:
                    _viewHolder.icon.setImageResource(R.drawable.btn_lookdown_activel2x);
                    break;
                case 3:
                    _viewHolder.icon.setImageResource(R.drawable.btn_broken_activel2x);
                    break;
                case 4:
                    _viewHolder.icon.setImageResource(R.drawable.btn_sad_activel2x);
                    break;
            }
        }
    }

    private class ViewHolder {
        private TextView content;
        private ImageView touch;
        private TextView goodjobcnt;
        private ImageView icon;
        private TextView username;
        private TextView datetime;
    }

    private void CreatDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setMessage("この投稿を削除しますか？");
        builder.setTitle("Message");
        builder.setPositiveButton("いいえ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setNegativeButton("はい", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (objIDList == null || objIDList.size() == 0) return;

                ParseInstallation installation = ParseInstallation.getCurrentInstallation().getCurrentInstallation();
                ParseQuery<ParseObject> query = ParseQuery.getQuery("GoodJob");
                query.getInBackground(objIDList.get(index), new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject object, ParseException e) {
                        if (e == null) {
                            object.deleteInBackground();
                        } else {

                        }
                    }
                });
            }
        });
        builder.create().show();
    }
}
