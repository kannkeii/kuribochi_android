package ambloom.kuribochi_android.model;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by kannkeii on 2016/01/06.
 */
public class MyAdapter extends BaseAdapter {

    private int mResource;
    private List<? extends Map<String, ?>> mSelfData;
    private String[] from;
    private int[] to;

    public MyAdapter(Context context, List<? extends Map<String, ?>> data, int resouce, String[] from, int[] to)
    {
        this.mSelfData = data;
        this.mResource = resouce;
        this.mSelfData = data;
        this.from = from;
        this.to = to;
    }


    @Override
    public int getCount() {
        return mSelfData.size();
    }

    @Override
    public Object getItem(int position) {
        return mSelfData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String get(int position,Objects key)
    {
        Map<String,?> map = (Map<String,?>)getItem(position);
        return map.get(key).toString();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
