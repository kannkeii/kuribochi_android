package ambloom.kuribochi_android.model;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.text.Layout;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.text.format.Time;
import android.widget.Toast;

import java.sql.Wrapper;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ambloom.kuribochi_android.R;
import ambloom.kuribochi_android.controller.MainActivity;

/**
 * Created by kannkeii on 2015/12/28.
 */
public class MyFragment extends Fragment{

    private Context mContext;
    private ListView listView;
    private ArrayList<String> usermessagelist,objIDList;
    private LayoutInflater inflater;
    private DataAdapter dataAdapter;
    private int scene,index = 0;
    private String userid;
    private boolean is_divpage;//ページごと処理するかどうか
    private static int pageNo = 0;//第１ページのデータを取得。最大値はサーバー上のデータページ/1ページのitem数
    private boolean canScroll = true;
    private boolean isRemark = false;//指がlistviewの初めてのitemに押したかどうか
    private int startY = 0;//画面上のListViewの一番上のitemは全list初めてのitemの場合、画面を押される時のy座標を記録
    private int mFirstVisibleItem = 0;//今画面上で見えるitemの一番上のitem
    private int currentState = 0;

    private static final int NORMAL = 0;//操作状態--一般状態
    private static final int PULL = 1;//操作状態--下へスクロールできる状態
    private static final int RELEASE = 2;//放すと更新できる状態
    private static final int REFRESHING = 3;//更新中

    ParseQuery<ParseObject> query;

    private int mScrollState = 0;//listview今のスクロール状態

//    public MyFragment(int _scene,String _id)
//    {
//        scene = _scene;
//        userid = _id;
//    }

    public MyFragment(){}


    public void setScene(int _scene)
    {
        scene = _scene;
    }

    public void setUserid(String _id)
    {
        userid = _id;
    }



//    public static MyFragment newInstance(int _scene,String _id)
//    {
//        MyFragment newFragment = new MyFragment();
//        scene = _scene;
//        userid = _id;
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_all, container, false);
        this.mContext = this.getActivity().getApplicationContext();
        this.listView = (ListView)view.findViewById(R.id.id_listview);

        ((MainActivity)this.getActivity()).registerMyTouchListener(myTouchListener);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                index = position;
                CreatDialog();
                return false;
            }
        });



        query = ParseQuery.getQuery("GoodJob");

        /*pageNoは修正できる*/
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {



            //SCROLL_STATE_IDLE=>スクロールが停止している場合
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                mScrollState = scrollState;

                if(scrollState == SCROLL_STATE_IDLE){
                    if(canScroll) {
                        int scrolled = view.getLastVisiblePosition();
                        canScroll = false;

//                        if(listView.getFirstVisiblePosition() == 0)
//                        {
//                            System.out.println("top!!!!!!!!");
//                        }

                            if(!is_divpage)return;
                            pageNo++;
                            //start = listView.getFirstVisiblePosition();
                            try {
                                query.setSkip(20 * pageNo);//どこまで読み込み
                                query.setLimit(20 * pageNo + 1);//どこまでデータを切る
                            } catch (ArrayIndexOutOfBoundsException e) {

                            }

                            /////////////////////////////////////////////////////////////
                            {
                                query.findInBackground(new FindCallback<ParseObject>() {
                                    @Override
                                    public void done(List<ParseObject> objects, ParseException e) {
                                        if (e == null) {
                                            for (ParseObject parseObject : objects) {
                                                String s1 = parseObject.get("text").toString();
                                                String s2 = parseObject.get("goodJob").toString();
                                                String s3 = parseObject.get("sex").toString();
                                                String s4 = parseObject.get("nickName").toString();
                                                if (TextUtils.isEmpty(s4)) s4 = "(´・ω・｀)さん";
                                                //else s4 += "さん";
                                                Date createtime = parseObject.getCreatedAt();
                                                String s5 = getDaytoChristmas(createtime);
                                                String sid = parseObject.getObjectId();

                                                //String s5 = parseObject.getCreatedAt().toString();

                                                usermessagelist.add(s1);
                                                usermessagelist.add(s2);
                                                usermessagelist.add(s3);
                                                usermessagelist.add(s4);
                                                usermessagelist.add(s5);
                                                objIDList.add(sid);
                                            }
                                            dataAdapter.notifyDataSetChanged();
                                            //listView.setSelectionFromTop(,start);
                                            //listView.setSelectionFromTop(scrolledy,20);
                                        }
                                    }
                                });
                            }
                            //////////////////////////////////////////////////////////////

                    }
                }else if(scrollState == SCROLL_STATE_TOUCH_SCROLL)
                {
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                is_divpage = (firstVisibleItem + visibleItemCount == totalItemCount);
                canScroll = true;
                mFirstVisibleItem = firstVisibleItem;
            }
        });




        switch (scene) {
            case 0:
                query.setLimit(20);
                query.orderByDescending("createdAt");
                //query.setLimit(10);
                //query.setSkip(10);
                break;
            case 1:
            case 2:
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                int scond = calendar.get(Calendar.SECOND);
                String start = year+"-"+(month+1)+"-"+(day-7)+" "+hour+":"+minute+":"+scond;//←日まで
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;
                try {
                    date = sdf.parse(start);
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                query.whereGreaterThanOrEqualTo("createdAt", date);//http://murayama.hatenablog.com/entry/2013/11/30/093741
                query.addDescendingOrder("goodJob");
                break;
            case 3:
                query.orderByDescending("goodJob");
                break;
            case 4:
                break;
            default:
                //System.out.println("orderByDescending値エラー:" + scene);
        }
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {

                    usermessagelist = new ArrayList<String>();
                    objIDList = new ArrayList<String>();
                    dataAdapter = new DataAdapter();
                    for (ParseObject parseObject : objects) {
                        String s1 = parseObject.get("text").toString();
                        String s2 = parseObject.get("goodJob").toString();
                        String s3 = parseObject.get("sex").toString();
                        String s4 = parseObject.get("nickName").toString();
                        if(TextUtils.isEmpty(s4))s4 = "(´・ω・｀)さん";
                        //else s4+="さん";
                        Date createtime = parseObject.getCreatedAt();
                        String s5 = getDaytoChristmas(createtime);
                        String sid = parseObject.getObjectId();

                        //String s5 = parseObject.getCreatedAt().toString();

                        usermessagelist.add(s1);
                        usermessagelist.add(s2);
                        usermessagelist.add(s3);
                        usermessagelist.add(s4);
                        usermessagelist.add(s5);
                        objIDList.add(sid);
                }
                    listView.setAdapter(dataAdapter);
                }
            }
        });
        return view;
    }

    private MainActivity.MyTouchListener myTouchListener = new MainActivity.MyTouchListener() {
        @Override
        public void onTouchEvent(MotionEvent event) {

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:// 手指按下
                    if(0 == mFirstVisibleItem) {//ListView当前屏幕可见的第一个item的位置
                        isRemark = true;//标识是否是在ListView的首个item出现在屏幕最顶端时手指按下
                        startY = (int) event.getY();//当前屏幕中，ListViwe显示的第一个item是首个item时，手指按下时的Y轴坐标
                    }
                    break;
                case MotionEvent.ACTION_MOVE:// 手指滑动屏幕
                    onMove(event);
                    break;
                case MotionEvent.ACTION_UP:// 手指抬起
                    if(currentState == RELEASE){//当前的操作状态为——松开可以刷新的状态
                        //currentState = REFRESHING;
                    }else if(currentState == PULL){// ＊（不存在）提示“下拉可以刷新”，那么就复位
                        currentState = NORMAL;//当前的操作状态为——正常状态
                        isRemark = false;//标识是否是在ListView的首个item出现在屏幕最顶端时手指按下
                    }
                    break;
                default:
                    break;
            }
        }
    };

    boolean doupdata = true;

    private void onMove(MotionEvent event)
    {
        if(!isRemark){//标识是否是在ListView的首个item出现在屏幕最顶端时手指按下
            return;
        }

        int currentY = (int)event.getY();//指がどこに移動したか
        int space = currentY -startY;//移動距離は

        //System.out.println(space);


        switch (currentState)//当前的操作状态
        {
            case NORMAL://当前的操作状态为——正常状态
                if(space>0){//移動距離は
                    //System.out.println("NORMAL");
                    currentState = PULL;//当前的操作状态
                }
                break;
            case PULL://当前的操作状态为——提示下拉可以刷新的状态
                if(space>30 && mScrollState == 1)//移動距離は //listview今のスクロール状態
                {//System.out.println("PULL");
                    currentState = RELEASE;//当前的操作状态
                }
                break;
            case RELEASE://当前的操作状态为——松开可以刷新的状态

                if(space<30){//移動距離は
                   // System.out.println("RELEASE1");
                    currentState = PULL;//当前的操作状态
                }else if(space <= 0){//移動距離は
                  //  System.out.println("RELEASE2");
                    currentState = NORMAL;//当前的操作状态
                    isRemark = false;//标识是否是在ListView的首个item出现在屏幕最顶端时手指按下
                }
                if(space>0)
                {
                    if(!doupdata) {
                        return;
                    }

                    switch (scene) {

                        case 0:
                        query.setLimit(20);
                        query.orderByDescending("createdAt");

                        query.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> objects, ParseException e) {

                                if (e == null) {
                                    doupdata = true;

                                    usermessagelist = new ArrayList<String>();
                                    objIDList = new ArrayList<String>();
                                    dataAdapter = new DataAdapter();
                                    for (ParseObject parseObject : objects) {
                                        String s1 = parseObject.get("text").toString();
                                        String s2 = parseObject.get("goodJob").toString();
                                        String s3 = parseObject.get("sex").toString();
                                        String s4 = parseObject.get("nickName").toString();
                                        if (TextUtils.isEmpty(s4)) s4 = "(´・ω・｀)さん";
                                        //else s4 += "さん";
                                        Date createtime = parseObject.getCreatedAt();
                                        String s5 = getDaytoChristmas(createtime);
                                        String sid = parseObject.getObjectId();

                                        //String s5 = parseObject.getCreatedAt().toString();

                                        usermessagelist.add(s1);
                                        usermessagelist.add(s2);
                                        usermessagelist.add(s3);
                                        usermessagelist.add(s4);
                                        usermessagelist.add(s5);
                                        objIDList.add(sid);
                                    }

                                    listView.setAdapter(dataAdapter);

                                }
                            }
                        });

                        if (doupdata) {
                            doupdata = false;
                        }
                            break;
                        case 2:
                        case 3:
                            break;
                    }
                }
                break;
            default:
                break;
        }
    }

    public static String getDaytoChristmas(Date _day)
    {
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        long day = 0;
        String returnStr = null;
        try{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(_day);
            Date christmasDay = myFormatter.parse(calendar.get(Calendar.YEAR) + "-12-25");

            day = (_day.getTime()-christmasDay.getTime())/(24*60*60*1000);

            if(day>0)
            {
                returnStr = "クリスマス経過して" + Math.abs(day) + "日";
            }else if(day<0)
            {
                returnStr = "クリスマスまで" + Math.abs(day) + "日";
            }else if(day == 0)
            {
                returnStr = "クリスマスディナー";
            }

        } catch (java.text.ParseException e) {
            return "";
        }
        return returnStr;
    }

    public class DataAdapter extends BaseAdapter {

        public DataAdapter() {

            inflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return usermessagelist.size()/5;
        }

        @Override
        public Object getItem(int position) {
            return usermessagelist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            ViewHolder viewHolder = null;
            index = position;
            final ImageButton imageButton;

            if (view == null) {
                viewHolder = new ViewHolder();
                view = inflater.inflate(R.layout.layout_item, parent, false);
                viewHolder.content = (TextView) view.findViewById(R.id.id_messagetext);
                viewHolder.goodjobcnt = (TextView)view.findViewById(R.id.id_goodcnt);
                viewHolder.touch =  (ImageView) view.findViewById(R.id.id_goodbuttom);
                viewHolder.icon = (ImageView)view.findViewById(R.id.id_userid);
                viewHolder.username = (TextView)view.findViewById(R.id.id_username);
                viewHolder.datetime = (TextView)view.findViewById(R.id.id_ymd);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }


            if(view != null) {
                int cnt = position + position * 4;//ViewHolder内のメンバー数-1(iconを抜いて)-1
                viewHolder.content.setText(usermessagelist.get(cnt));
                viewHolder.touch.setImageResource(R.drawable.goodjobl2x);
                viewHolder.goodjobcnt.setText(usermessagelist.get(cnt + 1));
                setIcon(cnt, viewHolder);
                viewHolder.username.setText(usermessagelist.get(cnt + 3));
                viewHolder.datetime.setText(usermessagelist.get(cnt + 4));
            }

            imageButton = (ImageButton)view.findViewById(R.id.id_goodbuttom);
            imageButton.setTag(position);

            final View finalView = view;
            imageButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (objIDList == null || objIDList.size() == 0) return;

                   index = Integer.parseInt(imageButton.getTag().toString());

                    ParseQuery<ParseObject> query = ParseQuery.getQuery("GoodJob");
                    query.getInBackground(objIDList.get(index), new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject object, ParseException e) {
                            if (e == null) {
                                String name = object.get("userName").toString();
                                if( name.equals(userid))return;
                                String str = object.get("goodJob").toString();
                                int goodJob = Integer.parseInt(str);
                                goodJob++;
                                object.put("goodJob", goodJob);
                                object.saveInBackground();
                                //View view2 = inflater.inflate(R.layout.layout_item,null);
                                Button button = (Button) finalView.findViewById(R.id.id_goodcnt);

                                button.setText(Integer.toString(goodJob));

                                showToast();

                            } else {

                            }

                        }

                    });

                }
            });

            return view;
        }

        private void setIcon(int _cnt,ViewHolder _viewHolder)
        {
            String s = usermessagelist.get(_cnt + 2);
            int cnt = Integer.parseInt(s);
            switch (cnt)
            {
                case 0:
                    _viewHolder.icon.setImageResource(R.drawable.btn_man_activel2x);
                    break;
                case 1:
                    _viewHolder.icon.setImageResource(R.drawable.btn_woman_activel2x);
                    break;
                case 2:
                    _viewHolder.icon.setImageResource(R.drawable.btn_lookdown_activel2x);
                    break;
                case 3:
                    _viewHolder.icon.setImageResource(R.drawable.btn_broken_activel2x);
                    break;
                case 4:
                    _viewHolder.icon.setImageResource(R.drawable.btn_sad_activel2x);
                    break;
            }
        }
    }

    private void  showToast()
    {
        int w,h;
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View layout = inflater.inflate(R.layout.anim_layout,null);
        ImageView imageView = (ImageView)layout.findViewById(R.id.tvImageToast);
        //imageView.setImageResource(R.drawable.toast_img1l2x);
        imageView.setBackgroundResource(R.drawable.anim);
        AnimationDrawable animationDrawable = (AnimationDrawable)imageView.getBackground();
        animationDrawable.setOneShot(true);
        animationDrawable.start();

        //if(animationDrawable.isRunning())System.out.println("RUN!!!!!!!!!!!!!");

        TextView text = (TextView)layout.findViewById(R.id.tvTextToast);
        text.setText("(´・ω・｀)");
        text.setTextColor(Color.WHITE);

        Toast toast;
        toast = new Toast(getActivity());
        toast.setGravity(Gravity.CENTER,12,40);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }



    private class ViewHolder {
        private TextView content;
        private ImageView touch;
        private TextView goodjobcnt;
        private ImageView icon;
        private TextView username;
        private TextView datetime;
    }

    private void CreatDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setMessage("この投稿を不適切なコンテンツとして報告しますか？");
        builder.setTitle("Message");
        builder.setPositiveButton("いいえ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //System.out.println();
            }
        });
        builder.setNegativeButton("はい", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (objIDList == null || objIDList.size() == 0) return;
                ParseInstallation installation = ParseInstallation.getCurrentInstallation().getCurrentInstallation();
                ParseQuery<ParseObject> query = ParseQuery.getQuery("GoodJob");
                query.getInBackground(objIDList.get(index), new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject object, ParseException e) {
                        if (e == null) {
                            String name = object.get("userName").toString();
                            if (name.equals(userid)) {
                                Toast toast = Toast.makeText(getActivity(), "自分を通報できません", Toast.LENGTH_SHORT);
                                //.setGravity(Gravity.BOTTOM | Gravity, 0, 0);
                                toast.show();
                                return;
                            }
                            String str = object.get("badContents").toString();
                            int badContents = Integer.parseInt(str);
                            badContents++;

                            object.put("badContents", badContents);
                            object.saveInBackground();

                            Toast toast = Toast.makeText(getActivity(), "通報しました", Toast.LENGTH_SHORT);
                            toast.show();
                        } else {

                        }
                    }
                });
            }
        });
        builder.create().show();
    }
}
