package ambloom.kuribochi_android.controller;

import android.animation.AnimatorSet;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import ambloom.kuribochi_android.R;

/**
 * Created by kannkeii on 2016/01/14.
 */
public class SendMessageActivity extends Activity implements View.OnClickListener{

    EditText mMessageBox;
    EditText mNameBox;
    RadioGroup mIconGrop;
    RadioButton mIcon0,mIcon1,mIcon2,mIcon3,mIcon4;
    ImageButton mSend,mClose;
    String mMessage,mName,mUserID;
    int mIconChosed = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_send);

        Bundle bundle = this.getIntent().getExtras();
        mUserID = bundle.getString("UserID");

        mMessageBox = (EditText)findViewById(R.id.id_user_comment);
        mNameBox = (EditText)findViewById(R.id.id_user_nameinput);
        mIconGrop = (RadioGroup)findViewById(R.id.id_iconGroup);
        mIcon0 = (RadioButton)findViewById(R.id.id_icon_button_0);
        mIcon1 = (RadioButton)findViewById(R.id.id_icon_button_1);
        mIcon2 = (RadioButton)findViewById(R.id.id_icon_button_2);
        mIcon3 = (RadioButton)findViewById(R.id.id_icon_button_3);
        mIcon4 = (RadioButton)findViewById(R.id.id_icon_button_4);
        mSend = (ImageButton)findViewById(R.id.id_send);
        mClose = (ImageButton)findViewById(R.id.id_close);

        mSend.setOnClickListener(this);
        mClose.setOnClickListener(this);

        IconGropListener();
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.id_send:
                getMessages();
                if(TextUtils.isEmpty(mMessage)) {
                    CreatDialog();
                    break;
                }

                ParseObject parseObject = new ParseObject("GoodJob");


                parseObject.put("nickName",mName);
                parseObject.put("sex",mIconChosed);
                parseObject.put("text",mMessage);
                parseObject.put("badContents",0);
                parseObject.put("userName",mUserID);

                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                String strTime = year+"/"+(month+1)+"/"+day+" "+hour+":"+minute;
                parseObject.put("dateTime",strTime);
                parseObject.put("goodJob", 0);
                parseObject.saveInBackground();
                this.finish();

                //System.out.println("結果===>" + mMessage + "|" + mName + "|"+mIconChosed);
                break;
            case R.id.id_close:
                this.finish();
                break;
            default:
                break;
        }
    }

    private void getMessages()
    {
        mMessage = mMessageBox.getText().toString();
        mName = mNameBox.getText().toString();

        /*先頭空白を削除*/
        mMessage = mMessage.trim();
    }

    private void IconGropListener()
    {
        mIconGrop.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == mIcon0.getId())
                {
                    mIconChosed = 0;
                }else if(checkedId == mIcon1.getId())
                {
                    mIconChosed = 1;
                }else if(checkedId == mIcon2.getId())
                {
                    mIconChosed = 2;
                }else if(checkedId == mIcon3.getId())
                {
                    mIconChosed = 3;
                }else if(checkedId == mIcon4.getId())
                {
                    mIconChosed = 4;
                }
            }
        });
    }

    private void CreatDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("あなたの(´・ω・｀)な出来事を入力してください。");
        builder.setTitle("Message");
        builder.setPositiveButton("OK!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SendMessageActivity.this.finish();
            }
        });
        builder.create().show();
    }
}
