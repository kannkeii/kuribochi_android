package ambloom.kuribochi_android.controller;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

import ambloom.kuribochi_android.model.MyFragment;
import ambloom.kuribochi_android.R;
import ambloom.kuribochi_android.model.MyFragment2;
import ambloom.kuribochi_android.model.SegmentedGroup;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Handler handler;
    private MyFragment mMyFragment;
    private MyFragment2 mMyFragment2;
    private PagerAdapter mAdapter;
    private List<View> mViews = new ArrayList<View>();//Viewlist

    /*各ページのボタン*/
    private ImageButton mButtomNew;
    private ImageButton mButtomRanking;
    private ImageButton mButtomMypage;

    /*各ページボタン下の文字*/
    private TextView mButtomNewText;
    private TextView mButtomRankingText;
    private TextView mButtomMypageText;

    /*トープバー*/
    private TextView mTopBarText;

    /*ペンボタン*/
    private ImageButton mPenButtom;

    private String mUserID = null;

    private int oldScene=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ParseData();

//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
//
//        if(mUserID == null) mUserID = sp.getString("UserID",null);
//
//
//        while (mUserID == null) {
//            /*ObjectIDで一行のデータを取得して、処理★*/
//            ParseInstallation.getCurrentInstallation().saveInBackground();
//            ParseInstallation installation = ParseInstallation.getCurrentInstallation().getCurrentInstallation();
//            mUserID = installation.getObjectId();
//            sp.edit().putString("UserID",mUserID).commit();
//        }

        doFullScreen(true);
        Intent intent = getIntent();
        boolean firstStart = true;//intent.getBooleanExtra("friststart", true);

        final long waittime = 500;

        if (firstStart) {
            welcomeScene(waittime);
        } else lunchmain();
    }

    private void ParseData() {
        Parse.initialize(this, "hIfIrjbpwXksJENCLQs9MP39ECB39K7NUWpg0XAz",
                "EMijFjVkbGzz7IHteBtClnRvPybiW8uzTa17gcv3");
    }


    private void doFullScreen(boolean enable) {
        if (enable) {
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
            getWindow().setAttributes(lp);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        } else {
            WindowManager.LayoutParams attr = getWindow().getAttributes();
            attr.flags &= (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().setAttributes(attr);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    public void welcomeScene(final long _Waittime) {
        setContentView(R.layout.before_main);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    lunchmain();
                }
            }
        };

        Runnable runnable = new Runnable() {
            public void run() {
                try {
                    Thread.sleep(_Waittime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.sendEmptyMessage(1);
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private void lunchmain() {

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        if(mUserID == null) mUserID = sp.getString("UserID",null);
        if(mUserID == null)
        {
            Intent intent = new Intent(this,LicenseActivity.class);
            intent.putExtra("UserID", mUserID);
            startActivity(intent);
        }

        while (mUserID == null) {
            /*ObjectIDで一行のデータを取得して、処理★*/
            ParseInstallation.getCurrentInstallation().saveInBackground();
            ParseInstallation installation = ParseInstallation.getCurrentInstallation().getCurrentInstallation();
            mUserID = installation.getObjectId();
            sp.edit().putString("UserID",mUserID).commit();
        }

        doFullScreen(false);

        drawView();

        InitEvent();


    }

    public void penButtom_click(View view)
    {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        LicenseActivity licenseActivity = new LicenseActivity();
        licenseActivity.setCanUseFlg(sp.getBoolean("CanUseFlg", Boolean.parseBoolean(null)));

        if(licenseActivity.getCanUseFlg() == false)
        {
            Intent intent = new Intent(this,LicenseActivity.class);
            startActivity(intent);
        }else
        {
            Intent intent = new Intent(this,SendMessageActivity.class);
            intent.putExtra("UserID", mUserID);
            startActivity(intent);
        }
    }

    private void drawView() {

        setContentView(R.layout.activity_main);

        listView(0);

        /*各ページのボタン*/
        mButtomNew = (ImageButton) findViewById(R.id.id_Button_New);
        mButtomRanking = (ImageButton) findViewById(R.id.id_Button_Ranking);
        mButtomMypage = (ImageButton) findViewById(R.id.id_Button_Mypage);

        mButtomNewText = (TextView) findViewById(R.id.id_Button_New_text);
        mButtomRankingText = (TextView) findViewById(R.id.id_Button_Ranking_text);
        mButtomMypageText = (TextView) findViewById(R.id.id_Button_Mypage_text);


        /*トープバー*/
        mTopBarText = (TextView) findViewById(R.id.id_top_text);


        mAdapter = new PagerAdapter() {
            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView(mViews.get(position));
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                View view = mViews.get(position);
                container.addView(view);
                return view;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public int getCount() {
                return mViews.size();
            }
        };

    }

    private void listView(int _scene)
    {
        /*変換事前処理*/
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        //mMyFragment = new MyFragment(_scene,mUserID);
        mMyFragment = new MyFragment();
        mMyFragment.setScene(_scene);
        mMyFragment.setUserid(mUserID);
        fragmentTransaction.replace(R.id.id_fragment, mMyFragment);/*変化を入れて*/
        fragmentTransaction.commit();/*実行*/
    }

    private void listView2()
    {
        /*変換事前処理*/
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        //mMyFragment2 = new MyFragment2(mUserID);
        mMyFragment2 = new MyFragment2();
        mMyFragment2.setUserid(mUserID);
        fragmentTransaction.replace(R.id.id_fragment, mMyFragment2);/*変化を入れて*/
        fragmentTransaction.commit();/*実行*/
    }


    private void InitEvent() {

        mButtomNew.setOnClickListener(this);
        mButtomRanking.setOnClickListener(this);
        mButtomMypage.setOnClickListener(this);
        /*ページ初期化*/
        Initpage();
    }

    private void Initpage() {
        Intent intent = getIntent();
        int chosedNo = intent.getIntExtra("chosed", 0);

        switch (chosedNo) {
            case 0:
                mTopBarText.setText("新着一覧");
                mButtomNew.setImageResource(R.drawable.tab_new_onl2x);
                setBottomTextColor(R.id.id_Layout_New);
                break;
            case 1:
                //mTopBarText.setText("ランキング");
                mButtomRanking.setImageResource(R.drawable.tab_rank_onl2x);
                setBottomTextColor(R.id.id_Layout_Ranking);
                break;
            case 2:
                mTopBarText.setText("マイページ");
                mButtomMypage.setImageResource(R.drawable.tab_my_onl2x);
                setBottomTextColor(R.id.id_Layout_Mypage);
                break;
            default:
                break;
        }
    }


    @Override
    public void onClick(View view) {
        LinearLayout layout = (LinearLayout) findViewById(R.id.id_top_bar);
        RestImg();
        switch (view.getId()) {
            case R.id.id_Button_New:
                mButtomNew.setImageResource(R.drawable.tab_new_onl2x);
                if (oldScene == 0 && oldScene != 1 && oldScene != 2) {
                    break;
                }
                oldScene = 0;
                listView(0);
                layout.removeAllViews();
                getLayoutInflater().inflate(R.layout.top_bar, layout);
                mTopBarText = (TextView) findViewById(R.id.id_top_text);


                //mButtomNew.setImageResource(R.drawable.tab_new_onl2x);
                mTopBarText.setText("新着一覧");
                setBottomTextColor(R.id.id_Layout_New);
                break;
            case R.id.id_Button_Ranking:
                mButtomRanking.setImageResource(R.drawable.tab_rank_onl2x);
                if (oldScene == 1 && oldScene != 0 && oldScene != 2) {
                    break;
                }
                oldScene = 1;
                listView(1);
                //mButtomRanking.setImageResource(R.drawable.tab_rank_onl2x);
                mTopBarText.setText("");

                //変更したいレイアウトを取得する

                // レイアウトのビューをすべて削除する
                layout.removeAllViews();
                // レイアウトをR.layout.top_bar_radiobtmに変更する
                getLayoutInflater().inflate(R.layout.top_bar_radiobtm, layout);


                SegmentedGroup group = (SegmentedGroup) findViewById(R.id.id_Radio1);
                group.check(R.id.id_Radio1_left);

                group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        RadioButton radio1, radio2;
                        radio1 = (RadioButton) findViewById(R.id.id_Radio1_left);
                        radio2 = (RadioButton) findViewById(R.id.id_Radio1_Right);

                        if (checkedId == radio1.getId()) {
                            listView(2);
                            //System.out.println(radio1.getText());
                        } else if (checkedId == radio2.getId()) {
                            listView(3);
                            //System.out.println(radio2.getText());
                        }
                    }
                });

                setBottomTextColor(R.id.id_Layout_Ranking);
                break;
            case R.id.id_Button_Mypage:
                mButtomMypage.setImageResource(R.drawable.tab_my_onl2x);
                if (oldScene == 2 && oldScene != 0 && oldScene != 1){
                    break;
                }
                oldScene = 2;
                listView2();
                layout.removeAllViews();
                // レイアウトをR.layout.top_bar_radiobtmに変更する
                getLayoutInflater().inflate(R.layout.top_bar, layout);
                //LinearLayout layout1 = (LinearLayout)findViewById(R.id.id_top_bar_mypage);
                //setTopBarSize(layout,256);

                //View view1 = LayoutInflater.from(this).inflate(R.layout.top_bar_mypage, null);
                mTopBarText = (TextView) findViewById(R.id.id_top_text);
                //mButtomMypage.setImageResource(R.drawable.tab_my_onl2x);
                mTopBarText.setText("マイページ");
                setBottomTextColor(R.id.id_Layout_Mypage);
                break;
            default:
                break;
        }
    }

    private void setTopBarSize(LinearLayout _layout,int _dpSize)
    {
        final float scale = this.getResources().getDisplayMetrics().density;
        int height = (int) (_dpSize * scale + 0.5f);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,height);
        _layout.setLayoutParams(params);
    }

    private void setBottomTextColor(int _num) {
        switch (_num) {
            case R.id.id_Layout_New:
                mButtomNewText.setTextColor(Color.rgb(22, 93, 255));
                mButtomRankingText.setTextColor(Color.rgb(138, 138, 138));
                mButtomMypageText.setTextColor(Color.rgb(138, 138, 138));
                break;
            case R.id.id_Layout_Ranking:
                mButtomNewText.setTextColor(Color.rgb(138, 138, 138));
                mButtomRankingText.setTextColor(Color.rgb(22, 93, 255));
                mButtomMypageText.setTextColor(Color.rgb(138, 138, 138));
                break;
            case R.id.id_Layout_Mypage:
                mButtomNewText.setTextColor(Color.rgb(138, 138, 138));
                mButtomRankingText.setTextColor(Color.rgb(138, 138, 138));
                mButtomMypageText.setTextColor(Color.rgb(22, 93, 255));
                break;
        }

    }

    private void RestImg() {
        mButtomNew.setImageResource(R.drawable.tab_new_offl2x);
        mButtomRanking.setImageResource(R.drawable.tab_rank_offl2x);
        mButtomMypage.setImageResource(R.drawable.tab_my_offl2x);
    }

    public interface MyTouchListener
    {
        public void onTouchEvent(MotionEvent event);
    }

    private ArrayList<MyTouchListener> myTouchListeners = new ArrayList<MyTouchListener>();

    public void registerMyTouchListener(MyTouchListener listener)
    {
        myTouchListeners.add(listener);
    }

    public void unRegisterMyTouchListener(MyTouchListener listener)
    {
        myTouchListeners.remove(listener);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event){
        for (MyTouchListener listener:myTouchListeners){
            listener.onTouchEvent(event);
        }
        return super.dispatchTouchEvent(event);
    }
}