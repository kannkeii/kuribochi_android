package ambloom.kuribochi_android.controller;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

import ambloom.kuribochi_android.R;

/**
 * Created by kannkeii on 2016/02/05.
 */
public class LicenseActivity extends Activity implements View.OnClickListener{

    private TextView mBack;
    private TextView mAgree;
    private boolean mCanUse = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_license);

        mBack = (TextView)findViewById(R.id.id_license_back);
        mAgree = (TextView)findViewById(R.id.id_license_agree);

        mBack.setOnClickListener(this);
        mAgree.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        switch (view.getId())
        {
            case R.id.id_license_agree:
                mCanUse = true;
                break;
            case R.id.id_license_back:
                mCanUse = false;
                break;
            default:
                break;
        }
        sp.edit().putBoolean("CanUseFlg", mCanUse).commit();
        this.finish();
    }

    public void setCanUseFlg(boolean _flg)
    {
        mCanUse = _flg;
    }


    public boolean getCanUseFlg()
    {
        return mCanUse;
    }

}
